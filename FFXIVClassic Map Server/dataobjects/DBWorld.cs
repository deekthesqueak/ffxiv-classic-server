﻿namespace FFXIVClassic_Map_Server.dataobjects
{
    class DBWorld
    {
        public ushort id;
        public string Address;
        public ushort port;
        public ushort listPosition;
        public ushort population;
        public string name;
        public bool   isActive;
        public string motd;
    }
}
